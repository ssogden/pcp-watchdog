#!/bin/bash

################### Config Vars ###################
#__serviceArray=("sshd" "nosuchd")
#__apiKey="changeme"
#__orgSeverity="3"
#__objOwner="support"
#__besDest="http://bes.domain.tld"

source wd_service
__date=$(hwclock --show --utc)
__basename=$(basename $BASH_SOURCE)
################### End Config Vars ###################

alarm () {

   curl \
   --connect-timeout 5 \
   --max-time 10 \
   --retry 3 \
   --retry-delay 0 \
   --retry-max-time 10 \
   -H "Content-Type: application/json" \
   -H "Accept: application/json" \
   -d "{\"apiKey\":\"${__apiKey}\", \"AlarmName\": \"PCPWatchdog\", \"objectowner\": \"${__objOwner}\", \"domain\": \"$HOSTNAME\", \"origindatetime\": \"${__date}\", \"originseverity\": \"${__orgSeverity}\", \"Description\": \"$1 not running\"}" \
   -X POST "${__besDest}" \
   -i

    __status=$?
    [ ${__status} -eq 0 ] && 2>/dev/null || logger -s "${__basename}: POST request to ${__besDest} has failed to execute"

}

for i in "${!__serviceArray[@]}"; do
    if [[ $(pidof "${__serviceArray[$i]}" | wc -w) -eq 0 ]]; then
        logger -s "${__basename}: ${__serviceArray[$i]} not running" 
        alarm "${__serviceArray[$i]}" "not running"
    fi
done

exit 0

